/*! \file  touchTest2.c
 *
 *  \brief Simple touch test
 *
 * Draw a bunch of targets and change color when touched
 *
 *
 *  \author jjmcd
 *  \date July 14, 2015, 7:30 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include "../include/graphicsHostLibrary.h"
#include "../include/colors.h"

/* Configuration fuses */
#pragma config FNOSC = FRCPLL       // Fast RC oscillator with PLL
#pragma config FWDTEN = OFF         // Turn off watchdog timer

#define BACKGROUNDCOLOR BLUE
#define TOUCHEDCOLOR RED
#define UNTOUCHEDCOLOR FORESTGREEN
#define BORDERCOLOR GOLD

#define NUMCIRCLES 12
#define CIRCLESIZE 25

/* Horizontal position of each circle */
int xc[NUMCIRCLES] = {
      70, 160, 250,
      70, 160, 250,
      70, 160, 250,
      70, 160, 250 };
/* Vertical position of each circle */
int yc[NUMCIRCLES] = {
      30, 30, 30,
      90, 90, 90,
      150, 150, 150,
      210, 210, 210 };
/* Memory of which circles touched */
int cPushed[NUMCIRCLES] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

/*! Draw a circle with border, color depends on if pushed */
void drawCircle(int nCircleNum)
{
  if (cPushed[nCircleNum])
    {
      TFTsetColorX(TOUCHEDCOLOR);
    }
  else
    TFTsetColorX(UNTOUCHEDCOLOR);
  TFTfillCircle(xc[nCircleNum], yc[nCircleNum], CIRCLESIZE);
  TFTsetColorX(BORDERCOLOR);
  TFTcircle(xc[nCircleNum], yc[nCircleNum], CIRCLESIZE);
  TFTcircle(xc[nCircleNum], yc[nCircleNum], CIRCLESIZE-1);
}

/* Flash the LED a bunch of times so we know when things start */
void flashyFlashy( void )
{
  int i;

  // LED flashing lets me know when the application actually
  // starts sending data.  LED is on a second, flashed quickly
  // 5 times, stays on a second, goes off a second, then data starts

  _LATB9 = 1;
  delay(1000);

  for (i = 0; i < 5; i++)
    {
      _LATB9 = 0;
      delay(100);
      _LATB9 = 1;
      delay(100);
    }
  delay(1000);

  _LATB9 = 0;
  delay(1000);
}

/*! main - Very simple touch test */

/*! Draws nine circles.  Then begins testing if any circles are
 *  touched.  If they are, the previously touched circle is redrawn
 *  in the un-touched color and the newly touched circle drawn in
 *  the touched color.
 */
int main(void)
{
  int i;
  unsigned int x, y;
  int nThis;

  // LED will be used for general signaling
  _TRISB9 = 0;

  serialInitialize(0);

  flashyFlashy();

  TFTinit(TFTLANDSCAPE);
  TFTsetBackColorX(BACKGROUNDCOLOR);
  TFTclear();

  for (i = 0; i < NUMCIRCLES; i++)
    drawCircle(i);

  while (1)
    {
      /* Check to see whether there is touch data */
      if (TFTgetTouch(&x, &y))
        {
          //delay(2000);
          nThis = 999;
          /* See if the touch is in the rectangle containing a circle */
          for (i = 0; i < NUMCIRCLES; i++)
            {
              if (x > (xc[i] - CIRCLESIZE))
                if (x < (xc[i] + CIRCLESIZE))
                  if (y > (yc[i] - CIRCLESIZE))
                    if (y < (yc[i] + CIRCLESIZE))
                      {
                        nThis = i;
                      }
            }
          /* Find the previously pushed circle */
          for (i = 0; i<NUMCIRCLES; i++)
            {
              /* Un-push old pushed circle */
              if (cPushed[i])
                {
                  if ( i!=nThis ) /* Don't redraw if unnecessary */
                    {
                      cPushed[i] = 0;
                      drawCircle(i);
                    }
                }
            }
          /* Make the new circle pushed and redraw */
          if ( nThis<100 )
            {
              cPushed[nThis] = 1;
              drawCircle(nThis);
            }
        }
      //delay(2000);
    }
  return 0;
}
